package com.voyager.tools.jira.pojo;

import lombok.Data;

/**
 * @author voyager.fu
 * @date 2020/11/20
 */
@Data
public class JiraPostData {
    private String        webhookEvent;
    private JiraUser      user;
    private JiraIssue     issue;
    private JiraChangelog changelog;
}
