package com.voyager.tools.jira.pojo;

import lombok.Data;

/**
 * @author voyager.fu
 * @date 2020/11/20
 */
@Data
public class Item {
    private String field;
    private String from;
    private String fromString;
    private String to;
    private String toString;
}
