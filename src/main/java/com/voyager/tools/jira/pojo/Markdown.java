package com.voyager.tools.jira.pojo;

import lombok.Data;

/**
 * @author voyager.fu
 * @date 2020/11/20
 */
@Data
public class Markdown {
    private String title;
    private String text;
}
