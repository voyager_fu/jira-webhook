package com.voyager.tools.jira.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * @author voyager.fu
 * @date 2020/11/20
 */
@Data
public class At {
    private List<String> atMobiles = new ArrayList<>();
    private boolean      isAtAll   = false;
}
