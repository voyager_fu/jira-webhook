package com.voyager.tools.jira.pojo;

import lombok.Data;

/**
 * @author voyager.fu
 * @date 2020/11/20
 */
@Data
public class JiraUser {
    private String key;
    private String displayName;
}
