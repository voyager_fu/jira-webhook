package com.voyager.tools.jira.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * @author voyager.fu
 * @date 2020/11/20
 */
@Data
public class JiraChangelog {
    private String id;
    private List<Item> items = new ArrayList<>();
}
